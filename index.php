<?php /* Template Name: Pagina por defecto */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

 <!-- Content -->

      <!-- portada-->
      <div class="portada__bg" style="background-image: url('<?= InPage::imgMod('portada_fondo',''); ?>');">
      	<div class="portada__bg_rgba">
      		<div class="container__salamanca">
      			<div class="portada__box d-flex align-items-end flex-column">
      				<div class="mt-auto">
      					<h2 class="text-right portada__font"><?=InPage::__('title_home','Un hostal<br>para sentirse en casa')?></h2>
      				</div>
      			</div>
      			<div class="portada__box_arrow d-flex align-items-center flex-column mx-auto">
      				<a href="#abajo" class="portada__arrow_link"><i class="fas fa-angle-down portada__arrow"></i></a>
      			</div>
      		</div>
      	</div>
      </div>      <!--/portada-->

      <!-- section 1 -->
      <div class="container__salamanca">
      	<div class="title__box">
      		<h2 class="title__font"><?=InPage::__('home_title_section_1','Estilo cercanía y confort<br>en el barrio más artístico de Salamanca')?></h2>
      		<div class="title__line"></div>
      	</div>
      </div><div class="container__salamanca">
      	<div class="container d-flex justify-content-center flex-column confort__padding">
      		<p class="text-justify"><?=InPage::__('home_text_1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident.<br>')?></p>
      		<p class="text-justify"><?=InPage::__('home_text_2','<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat.')?></p>
      		<a class="text-center button__btn" href="#button"><?=InPage::__('home_btn_text','Ver habitaciones')?></a>	</div>
      </div>      <!-- /section 1 -->

      <!-- section 2 -->
      <div class="reserva__bg">
      	<div class="container__salamanca">
      		<div class="reserva__line d-flex mx-auto"></div>
      		<div class="d-flex flex-wrap">
      			<div class="col-lg-4 col-md-4 col-12 reserva__padding">
      				<div class="d-flex flex-column">
      					<div class="reserva__box_img mb-4">
      						<img src="<?= InPage::imgMod('home_seccion_2_habitacion_1','habitacion-1.jpg'); ?>" class="reserva__img_1" alt="">
      					</div>
      					<div class="reserva__box_img_1 reserva__bg_text reserva__box_content d-flex justify-content-center flex-column">
      						<h3 class="reserva__title"><?=InPage::__('home_title_1_section_2','Loremn ipsum Odor')?></h3>
      						<div class="reserva__line_2"></div>
      						<p class="reserva__font mb-0 text-justify"><?=InPage::__('home_text_1_section_2','Lorem ipsum dolor sit amet, consectetur adipisicing elit, elit elit elit sed do eiusmoorem ipsum dolor sit amet, consectetur.')?></p>
      					</div>
      				</div>
      			</div>
      			<div class="col-lg-4 col-md-4 col-12 reserva__padding">
      				<div class="d-flex flex-column">
      					<div class="reserva__box_img mb-4">
      						<img src="<?= InPage::imgMod('home_seccion_2_habitacion_2','habitacion-3.jpg'); ?>" class="reserva__img_1" alt="">
      					</div>
      					<div class="reserva__box_img_1 reserva__none">
      						<img src="<?= InPage::imgMod('home_seccion_2_habitacion_3','habitacion-2.jpg'); ?>" class="reserva__img_2" alt="">
      					</div>
      				</div>
      			</div>
      			<div class="col-lg-4 col-md-4 col-12 reserva__padding">
      				<div class="reserva__text">
      					<div class="reserva__box_text_2 d-flex">
      						<div class="my-auto reserva__box_content">
      							<h3 class="reserva__title"><?=InPage::__('home_title_section_2','Loremn ipsum Odor')?></h3>
      							<div class="reserva__line_2"></div>
      							<p class="reserva__font mb-0 text-justify"><?=InPage::__('home_text_section_2','Lorem ipsum dolor sit amet, consectetur adipisicing elit, elit elit elit sed do eiusmoorem ipsum dolor sit amet, consectetur.')?></p>
      						</div>
      					</div>
      				</div>
      			</div>
      			<a href="#btn" class="text-center mx-auto reserva__btn"><?=InPage::__('home_btn_section_2','Reserva ya')?></a>
      		</div>
      		<div class="reserva__line_final d-flex mx-auto mt-lg-2"></div>
      	</div>
      </div>      <!-- /section 2 -->
      <!-- section 3 -->
      <div class="container__salamanca">
      	<div class="icon__bg">
      		<div class="mx-auto icon__line"></div>
      		<div class="d-flex flex-lg-nowrap flex-wrap justify-content-center icon__padding">
      			<div class="text-center col-lg-3 col-md-6 col-sm-6 col-12 icon__movil">
      				<i class="fas fa-map-marker-alt icon__fonts"></i>
      				<h3 class="icon__text_title"><?=InPage::__('home_title_section_3','CENTRICO')?></h3>
      				<p class="icon__text_parrofo"><?=InPage::__('home_parrafo_section_3','A diez minutos andando del centro historico.')?></p>
      			</div>
      			<div class="text-center col-lg-3 col-md-6 col-sm-6 col-12 icon__movil">
      				<i class="fas fa-wheelchair icon__fonts"></i>
      				<h3 class="icon__text_title"><?=InPage::__('home_title_section_3_1','PARA TODOS')?></h3>
      				<p class="icon__text_parrofo"><?=InPage::__('home_parrafo_1_section_3','Con baños e instalaciones adaptadas.')?></p>
      			</div>
      			<div class="text-center col-lg-3 col-md-6 col-sm-6 col-12 icon__movil">
      				<i class="far fa-star icon__fonts"></i>
      				<h3 class="icon__text_title"><?=InPage::__('home_title_section_3_2','CON ESTILO')?></h3>
      				<p class="icon__text_parrofo"><?=InPage::__('home_parrafo_2_section_3','Cuidamos cada detalle y cada habitación.')?></p>
      			</div>
      			<div class="text-center col-lg-3 col-md-6 col-sm-6 col-12 icon__movil">
      				<i class="fas fa-bed icon__fonts"></i>
      				<h3 class="icon__text_title"><?=InPage::__('home_title_section_3_3','COMO EN CASA')?></h3>
      				<p class="icon__text_parrofo"><?=InPage::__('home_parrafo_3_section_3','Suite, salón, baño privado, cafetera... ¿te falta algo?')?></p>
      			</div>
      		</div>
      	</div>
      </div>      <!-- /section 3 -->
      <!-- section 3 -->
      <div class="collage__bg position-relative" id="abajo">
      	<img src="<?= InPage::imgMod('home_seccion_3_collage_1','collage-1.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_2','collage-2.jpg'); ?>" class="collage__img" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_3','collage-3.jpeg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_4','collage-4.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_5','collage-5.png'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_6','collage-6.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_7','collage-7.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_8','collage-8.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_9','collage-9.jpg'); ?>" class="collage__img" alt="">
        	<img src="<?= InPage::imgMod('home_seccion_3_collage_10','collage-10.jpg'); ?>" class="collage__img" alt="">
      	<a href="#" class="collage__btn text-center"><?=InPage::__('home_btn_section_4','Descubre Salamanca')?></a>
      </div>      <!-- /section 3 -->

    <!-- Content -->

<?php get_footer(); ?>

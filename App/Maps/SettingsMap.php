<?php namespace App\Maps;
/*
 *
 */
trait SettingsMap {
  // Los items son "paneles"
  protected $items = [

      # Panel del Layout
      'panel_layout' => [
        'title'         => 'Layout',
        'description'   => 'Configuraciones del layout',
        'priority'      => 1,
        'sections' => [
            'seccion_navbar' => [
              'title'         =>  'Barra de navegacion',
              'description'   =>  'Barra de navegacion de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'navbar_logo' => [

              ]
            ],
            'seccion_footer' => [
              'title'         =>  'Footer',
              'description'   =>  'Footer de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'footer_fondo' => [
                    'title'   => 'Fondo del footer',
                    'type'    => 'image',
                    'default' => ''
                  ],
              ]
            ]
        ]
    ],
    'panel_home' => [
        'title'         => 'Home',
        'description'   => 'Configuraciones de la home',
        'priority'      => 1,
        'sections' => [
            'seccion_header' => [
              'title'         =>  'Header',
              'description'   =>  'Header de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'portada_fondo' => [
                    'title'   => 'Imagen de fondo',
                    'type'    => 'image',
                    'default' => ''
                  ],
              ]
            ],
            'home_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
            ],
            'home_seccion_2' => [
              'title'         =>  'Seccion 2',
              'description'   =>  'Seccion 2 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'home_seccion_2_habitacion_1' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'habitacion-1.jpg'
                  ],
                  'home_seccion_2_habitacion_2' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'habitacion-2.jpg'
                  ],
                  'home_seccion_2_habitacion_3' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'habitacion-3.jpg'
                  ],      
              ]
            ],
            'home_seccion_3' => [
              'title'         =>  'Seccion 3',
              'description'   =>  'Seccion 3 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'home_seccion_3_collage_1' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-1.jpg'
                  ],
                  'home_seccion_3_collage_2' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-2.jpg'
                  ],
                  'home_seccion_3_collage_3' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-3.jpeg'
                  ],
                  'home_seccion_3_collage_4' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-4.jpg'
                  ],
                  'home_seccion_3_collage_5' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-5.png'
                  ],
                  'home_seccion_3_collage_6' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-6.jpg'
                  ],
                  'home_seccion_3_collage_7' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-7.jpg'
                  ],
                  'home_seccion_3_collage_8' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-8.jpg'
                  ],
                  'home_seccion_3_collage_9' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-9.jpg'
                  ],
                  'home_seccion_3_collage_10' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-10.jpg'
                  ],            
              ]
            ],
            'home_seccion_4' => [
              'title'         =>  'Seccion 4',
              'description'   =>  'Seccion 4 de la pagina',
              'priority'      =>  2,
              'inputs'        => [           
              ]
            ],
        ]
    ],
    'panel_room' => [
        'title'         => 'Room',
        'description'   => 'Configuraciones de la room',
        'priority'      => 1,
        'sections' => [
            'seccion_header' => [
              'title'         =>  'Header',
              'description'   =>  'Header de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'background_rojas' => [
                    'title'   => 'Imagen de fondo',
                    'type'    => 'image',
                    'default' => ''
                  ],
              ]
            ],
            'room_portada' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'background_rojas' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => ''
                  ],
                  'room_portada_suites_1' => [
                    'title'   => 'suites 1',
                    'type'    => 'image',
                    'default' => 'suites-1.jpeg'
                  ],
                  'room_portada_suites_2' => [
                    'title'   => 'suites 2',
                    'type'    => 'image',
                    'default' => 'suites-2.jpg'
                  ],
              ]
            ],
            'room_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'room_seccion_1_fondo' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => ''
                  ],
                  'room_seccion_1_icon_1' => [
                    'title'   => 'Icon png 1',
                    'type'    => 'image',
                    'default' => 'icon-4.png'
                  ],
                  'room_seccion_1_icon_2' => [
                    'title'   => 'Icon png 2',
                    'type'    => 'image',
                    'default' => 'icon-2.png'
                  ],
                  'room_seccion_1_icon_3' => [
                    'title'   => 'Icon png 3',
                    'type'    => 'image',
                    'default' => 'icon-3.png'
                  ],
              ]
            ],
            'room_seccion_2' => [
              'title'         =>  'Seccion 2',
              'description'   =>  'Seccion 2 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'room_seccion_2_collage_1' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-9.jpg'
                  ],
                  'room_seccion_2_collage_2' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-2.jpg'
                  ],
                  'room_seccion_2_collage_3' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-10.jpg'
                  ],      
              ]
            ],
        ]
    ],
    'panel_rooms' => [
        'title'         => 'Rooms',
        'description'   => 'Configuraciones de la pagina Rooms',
        'priority'      => 1,
        'sections' => [
          'rooms_portada' => [
              'title'         =>  'Portada',
              'description'   =>  'Portada de la pagina',
              'priority'      =>  1,
              'inputs'        => [
              ]
            ],
            'rooms_section_1' => [
              'title'         =>  'Sección 1',
              'description'   =>  'Sección 1 de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'rooms_habitaciones_1' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'habitacion-3.jpg'
                  ],
                  'rooms_habitaciones_2' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'habitacion-2.jpg'
                  ],
                  'rooms_icon_1' => [
                    'title'   => 'Icon png',
                    'type'    => 'image',
                    'default' => 'icon-room-1.png'
                  ],
                  'rooms_icon_2' => [
                    'title'   => 'Icon png',
                    'type'    => 'image',
                    'default' => 'icon-room-2.png'
                  ],
                  'rooms_icon_3' => [
                    'title'   => 'Icon png',
                    'type'    => 'image',
                    'default' => 'icon-room-3.png'
                  ],
              ]
            ],
            'rooms_section_2' => [
              'title'         =>  'Sección 2',
              'description'   =>  'Sección 2 de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'rooms_collage_1' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-9.jpg'
                  ],
                  'rooms_collage_2' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-2.jpg'
                  ],
                  'rooms_collage_3' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-10.jpg'
                  ],
                  'rooms_collage_4' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-4.jpg'
                  ],
                  'rooms_collage_5' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-5.png'
                  ],
                  'rooms_collage_6' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-6.jpg'
                  ],
                  'rooms_collage_7' => [
                    'title'   => 'Imagen',
                    'type'    => 'image',
                    'default' => 'collage-7.jpg'
                  ],
              ]
            ],
            'rooms_section_3' => [
              'title'         =>  'Sección 3',
              'description'   =>  'Sección 3 de la pagina',
              'priority'      =>  1,
              'inputs'        => [
              ]
            ],
        ]
    ],
    // # Otros ajustes de plantilla
    // 'panel_others' => [
    //   'title'         => 'Otros ajustes',
    //   'description'   => 'Otros ajustes generales de plantilla',
    //   'priority'      => 1,
    //   'sections' => [
    //       'seccion_textos_generales' => [
    //         'title'         =>  'Textos generales',
    //         'description'   =>  'Textos generales de plantilla',
    //         'priority'      =>  1,
    //         'inputs'        => [
    //           // Solo tendra textos y estos se registraran en los StringsMap.php
    //         ]
    //       ],
    //       'seccion_imagenes_generales' => [
    //         'title'         =>  'Imagenes generales',
    //         'description'   =>  'Imagenes generales de la pagina',
    //         'priority'      =>  2,
    //         'inputs'        => [
    //           'favicon_page' => [
    //             'title'   => 'Imagen de favicon',
    //             'type'    => 'image',
    //             'default' => 'favicon.png'
    //           ],
    //         ]
    //       ],
    //   ]
    // ],

  ];
}

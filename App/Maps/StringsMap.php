<?php namespace App\Maps;
/*
 *
 */
trait StringsMap {
  protected $items = [
    'texto_404_not_found' => [
      'label' => 'Mensaje de pagina no encontrada (404)',
      'default'   => "La pagina que usted intenta buscar no existe!",
      'section' => 'seccion_textos_generales',
      'polyGroup' => 'Nextscale - Otras configuraciones'
    ],
    'home_titulo_sobre_nosotros' => [
      'label' => 'Mensaje de pagina no encontrada (404)',
      'default'   => "La pagina que usted intenta buscar no existe!",
      'section' => 'seccion_textos_generales',
      'polyGroup' => 'Nextscale - Otras configuraciones'
    ],
    'home_title' => [
      'label' => 'Titulo 1 de la portada',
      'default'   => "Un hostal<br>para sentirse en casa",
      'section' => 'seccion_header',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_title_section_1' => [
      'label' => 'Titulo 2 de la homepage',
      'default'   => "Estilo cercanía y confort<br>en el barrio más artístico de Salamanca",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_text_1' => [
      'label' => 'Texto del titulo de la seccion 1',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident.<br>",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home'
    ],
    'home_text_2' => [
      'label' => 'Texto 2 del titulo de la seccion 1',
      'default'   => "<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home'
    ],
    'home_btn_text' => [
      'label' => 'Texto del boton de la seccion 1 de home',
      'default'   => "Ver habitaciones",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home'
    ],
    'home_title_section_2' => [
      'label' => 'Titulo 2 de la seccion 2',
      'default'   => "Loremn ipsum Odor",
      'section' => 'home_seccion_2',
      'polyGroup' => 'Nextscale - Home'
    ],
    'home_text_section_2' => [
      'label' => 'Texto 2 de la seccion 2',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, elit elit elit sed do eiusmoorem ipsum dolor sit amet, consectetur.",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'home_seccion_2',
      'type' => 'textarea'
    ],
    'home_title_1_section_2' => [
      'label' => 'Titulo de la seccion 2',
      'default'   => "Loremn ipsum Odor",
      'section' => 'home_seccion_2',
      'polyGroup' => 'Nextscale - Home'
    ],
    'home_text_1_section_2' => [
      'label' => 'Texto de la seccion 2',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, elit elit elit sed do eiusmoorem ipsum dolor sit amet, consectetur.",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'home_seccion_2',
      'type' => 'textarea'
    ],
    'home_title_section_3' => [
      'label' => 'Titulo 1 de seccion 3',
      'default'   => "CENTRICO",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_title_section_3_1' => [
      'label' => 'Titulo 2 de seccion 3',
      'default'   => "PARA TODOS",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_title_section_3_2' => [
      'label' => 'Titulo 3 de seccion 3',
      'default'   => "CON ESTILO",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_title_section_3_3' => [
      'label' => 'Titulo 3 de seccion 3',
      'default'   => "COMO EN CASA",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_parrafo_section_3' => [
      'label' => 'Texto 1 de seccion 3',
      'default'   => "A diez minutos andando del centro historico.",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_parrafo_1_section_3' => [
      'label' => 'Texto 2 de seccion 3',
      'default'   => "Con baños e instalaciones adaptadas.",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_parrafo_2_section_3' => [
      'label' => 'Texto 3 de seccion 3',
      'default'   => "Cuidamos cada detalle y cada habitación.",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_parrafo_3_section_3' => [
      'label' => 'Texto 4 de seccion 3',
      'default'   => "Suite, salón, baño privado, cafetera... ¿te falta algo?",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_text_1_section_1' => [
      'label' => 'Texto 1 de seccion 1',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. <br><br> Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborumculpa qui officia deser nt, sunt in culpa qui officiar.",
      'section' => 'home_seccion_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_btn_section_1' => [
      'label' => 'Texto del boton de room seccion 1',
      'default'   => "Reserva ya",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_title_2' => [
      'label' => 'Titulo 2  del la seccion 1 de room',
      'default'   => "Caraterísticas",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_subtitle_1' => [
      'label' => 'Subtitulo 1 de Caraterísticas ',
      'default'   => "Lorem ipsum",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_subtitle_2' => [
      'label' => 'Subtitulo 2 de Caraterísticas ',
      'default'   => "Lorem ipsum",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_subtitle_3' => [
      'label' => 'Subtitulo 3 de Caraterísticas ',
      'default'   => "Lorem ipsum",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_text_1' => [
      'label' => 'Texto de las Caraterísticas',
      'default'   => "Odor ipsu am",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_text_2' => [
      'label' => 'Texto 2 de las Caraterísticas',
      'default'   => "Odor ipsu am",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_text_3' => [
      'label' => 'Texto 3 de las Caraterísticas',
      'default'   => "Odor ipsu am",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_1_btn' => [
      'label' => 'Titulo del boton de la seccion 1',
      'default'   => "Reserva ya",
      'section' => 'room_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_title' => [
      'label' => 'Titulo de la seccion 2',
      'default'   => "¿Quieres ver mas habitacion?",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_subtitle_1' => [
      'label' => 'Titulo del la imagen de room',
      'default'   => "Rooms 1",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_subtitle_2' => [
      'label' => 'Titulo del la imagen de room',
      'default'   => "Rooms 2",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_subtitle_3' => [
      'label' => 'Titulo del la imagen de room',
      'default'   => "Rooms 3",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_number_1' => [
      'label' => 'Numero del la imagen de room',
      'default'   => "01",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_number_2' => [
      'label' => 'Numero del la imagen de room',
      'default'   => "02",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'room_section_2_number_3' => [
      'label' => 'Numero del la imagen de room',
      'default'   => "03",
      'section' => 'room_seccion_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_portada_title' => [
      'label' => 'Titulo de la portada rooms',
      'default'   => "descansa",
      'section' => 'rooms_portada',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_title' => [
      'label' => 'Titulo de la sección 1',
      'default'   => "¿Quieres lo mejor?<br>Tenemos lo que necesitas",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_title_2' => [
      'label' => 'Titulo 2 de la sección 1',
      'default'   => "Nuestras Suites",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_text_2' => [
      'label' => 'Texto 2 de la sección 1',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_icon_title_1' => [
      'label' => 'Titulo del icono',
      'default'   => "AMPLITUD",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_icon_title_2' => [
      'label' => 'Titulo del icono',
      'default'   => "PRIVADO",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
     'rooms_section_1_icon_title_3' => [
      'label' => 'Titulo del icono',
      'default'   => "Cama de X CM",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_1_btn_icons' => [
      'label' => 'Texto del boton',
      'default'   => "Reserva ya",
      'section' => 'rooms_section_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_2_title' => [
      'label' => 'Titulo de la seccion 2',
      'default'   => "¿Prefieres una habitación estandar?<br>te la enseñamos",
      'section' => 'rooms_section_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_2_title_1' => [
      'label' => 'Titulo de la seccion 2',
      'default'   => "Nuestras Habitaciones",
      'section' => 'rooms_section_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_2_text_1' => [
      'label' => 'Texto de la seccion 2',
      'default'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.",
      'section' => 'rooms_section_2',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_3_title' => [
      'label' => 'Texto de la seccion 3',
      'default'   => "Y además...",
      'section' => 'rooms_section_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'rooms_section_3_text' => [
      'label' => 'Texto de la seccion 3',
      'default'   => "Salon Comunitario<br>Cafetera y microondas<br>Baños adaptados para todos",
      'section' => 'rooms_section_3',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
  ];
}

const path = require('path');
// variable para cambiar la ruta de salida
var isProduction = false;
const production = (isProduction) ? '../../' : '';
const production_folder = (isProduction) ? '../../static/' : '';
const direction = (isProduction) ? '../' : '../static/';
// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
    entry: {
        app: './src/app.js',
        home: './src/js/home.js',
        rooms: './src/js/rooms.js',
        room: './src/js/room.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name]-bundle.js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/view/homepage.handlebars',
            filename: 'home.html',
            chunks: ['app', 'home']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/rooms.handlebars',
            filename: 'rooms.html',
            chunks: ['app', 'rooms']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/room.handlebars',
            filename: 'room.html',
            chunks: ['app', 'room']
        }),
        new MiniCssExtractPlugin({
            filename: production_folder + 'css/[name]-styles.css',
        })
    ],
    module: {
        rules: [{
            test: /\.(sa|sc|c)ss$/,
            use: [
                MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
            ]
        }, {
            test: /\.handlebars$/,
            loader: 'handlebars-loader'
        }, {
            test: /\.(jpg|png|gif|svg|jpeg)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: production + 'static/img',
                    publicPath: direction + 'img',
                    useRelativePath: true,
                }
            }]
        }, {
            test: /\.(ttf|otf|woff2)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: production + 'static/fonts',
                    publicPath: direction + 'fonts',
                    useRelativePath: false
                }
            }]
        }, {
            loader: 'image-webpack-loader', // Minificador de imagenes
            options: {
                mozjpeg: {
                    progressive: true,
                    quality: 65
                },
                // optipng.enabled: false will disable optipng
                optipng: {
                    enabled: true,
                },
                pngquant: {
                    quality: [0.65, 0.90],
                    speed: 4
                },
                gifsicle: {
                    interlaced: false,
                },
                // the webp option will enable WEBP
                webp: {
                    quality: 75
                }
            }
        }]
    }
};
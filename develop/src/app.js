//css
import './css/main/main.scss';
// Fonts
// Fonts
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';
// boostrap
import 'mdbootstrap/css/bootstrap.min.css';
import 'mdbootstrap/css/mdb.min.css';
import 'mdbootstrap/css/style.css';
import 'mdbootstrap/js/jquery.min.js';
import 'mdbootstrap/js/popper.min.js';
import 'mdbootstrap/js/bootstrap.min.js';
// import 'mdbootstrap/js/mdb.min.js';
//imgs
import './static/img/Logotipo.png';
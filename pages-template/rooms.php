<?php /* Template Name: Pagina rooms */ ?>
<?php /* Template description: Para jugar con elementor */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

<!-- Content -->

    <!-- portada-->
    <div class="portadas__bg">
        <div class="portadas__rgba">
            <div class="container__salamanca d-flex align-items-end flex-column h-100">
                <div class="portadas__boxtitle mt-auto">
                    <h2 class="portadas__title"><?=InPage::__('rooms_portada_title','descansa')?></h2>
                    <div class="portadas__line"></div>
                </div>
            </div>
        </div>
    </div>      
    <!--/portada-->

    <!-- section 1 -->
    <div class="container__salamanca">
        <div class="title__box">
            <h2 class="title__font"><?=InPage::__('rooms_section_1_title','¿Quieres lo mejor?<br>Tenemos lo que necesitas')?></h2>
            <div class="title__line"></div>
        </div>
    </div><div class="container__salamanca">
        <div class="d-flex flex-wrap">
            <div class="col-lg-4 pr-md-2 col-md-4 col-12 suites__box_col">
                <div class="position-relative suites__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_habitaciones_1','habitacion-3.jpg'); ?>" class="suites__imgs" alt="">
                    <a href="#" class="position-absolute suites__box_link">
                        <div class="p-3">
                            <p class="suites__number">01</p>
                            <div class="suites__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center suites__box_titles">
                            <h3 class="text-center text-white my-auto suites__fonts_titles">Suites Platas</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 px-md-2 col-md-4 col-12 suites__box_col">
                <div class="position-relative suites__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_habitaciones_2','habitacion-2.jpg'); ?>" class="suites__imgs" alt="">
                    <a href="#" class="position-absolute suites__box_link">
                        <div class="p-3">
                            <p class="suites__number">02</p>
                            <div class="suites__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center suites__box_titles">
                            <h3 class="text-center text-white my-auto suites__fonts_titles">Suites Platas</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 pl-md-2 col-md-4 col-12 suites__box_col_black">
                <div class="bg-dark suites__box_black d-flex">
                    <div class="my-auto suites__overflow">
                    <h4 class="text-white suites__title"><?=InPage::__('rooms_section_1_title_2','Nuestras Suites')?></h4>
                    <div class="suites__line"></div>
                    <p class="text-white text-justify suites__parrafo"><?=InPage::__('rooms_section_1_text_2','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et doloreonsectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut.')?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="suites__bg">
        <div class="container__salamanca suites__bg2">
            <div class="d-flex flex-nowrap suites__none">
            <div class="suites__col text-center">
                <img src="<?= InPage::imgMod('rooms_icon_1','icon-room-1.png'); ?>" class="suites__icon" alt="">
                <h4 class="suites__icon_font"><?=InPage::__('rooms_section_1_icon_title_1','AMPLITUD')?></h4>
            </div>
            <div class="suites__col text-center">
                <img src="<?= InPage::imgMod('rooms_icon_2','icon-room-2.png'); ?>" class="suites__icon" alt="">
                <h4 class="suites__icon_font"><?=InPage::__('rooms_section_1_icon_title_2','BAÑO PRIVADO')?></h4>
            </div>
            <div class="suites__col text-center">
                <img src="<?= InPage::imgMod('rooms_icon_3','icon-room-3.png'); ?>" class="suites__icon" alt="">
                <h4 class="suites__icon_font"><?=InPage::__('rooms_section_1_icon_title_3','Cama de X CM')?></h4>
            </div>
            </div>
            <div class="suites__box_btn">
                <a href="#reserva" class="suites__btn"><?=InPage::__('rooms_section_1_btn_icons','Reserva ya')?></a>
            </div>
        </div>
    </div>      
    <!-- /section 1 -->

    <!-- section 2 -->
    <div class="container__salamanca">
        <div class="habitaciones__box">
            <h2 class="habitaciones__font"><?=InPage::__('rooms_section_2_title','¿Prefieres una habitación estandar?<br>te la enseñamos')?></h2>
            <div class="habitaciones__line"></div>
        </div>
    </div>
    <div class="habitaciones__bg">
        <div class="container__salamanca">
            <div class="d-flex flex-wrap">
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col pr-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_1','collage-9.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">01</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 1</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col px-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_2','collage-2.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">02</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 2</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col pl-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_3','collage-10.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">03</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 3</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col pr-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_4','collage-4.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">04</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 3</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col px-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_5','collage-5.png'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">05</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 5</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col pl-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_6','collage-6.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">06</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 6</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col pr-md-2  col-12 my-2">
                <div class="position-relative habitaciones__box_imgs">
                    <img src="<?= InPage::imgMod('rooms_collage_7','collage-7.jpg'); ?>" class="habitaciones__imgs" alt="">
                    <a href="#" class="position-absolute habitaciones__box_link">
                        <div class="p-3">
                            <p class="habitaciones__number">07</p>
                            <div class="habitaciones__line_number"></div>
                        </div>
                        <div class="my-auto">
                            <div class="d-flex justify-content-center habitaciones__box_titles">
                            <h3 class="text-center text-white my-auto habitaciones__fonts_titles">Rooms 7</h3>
                        </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitaciones__col px-md-2  col-12 my-2">
                <div class="p-4 text-white habitaciones__box_text h-100">
                    <h3 class="habitaciones__title"><?=InPage::__('rooms_section_2_title_1','Nuestras Habitaciones')?></h3>
                    <div class="habitaciones__line_box"></div>
                    <p class="habitaciones__parrafo"><?=InPage::__('rooms_section_2_text_1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.')?></p>
                </div>
            </div>
        </div>
        </div>
    </div>      
    <!-- /section 2 -->

      <!-- section 3 -->
      <div class="salon__bg">
        <div class="salon__rgba">
            <div class="container__salamanca">
                <div class="d-flex align-items-end flex-column salon__box">
                  <div class="salon__title"><?=InPage::__('rooms_section_3_title','Y además...')?></div>
                  <div class="mt-auto text-white salon__parrafo"><p><?=InPage::__('rooms_section_3_text','Salon Comunitario<br>Cafetera y microondas<br>Baños adaptados para todos')?></p>
                  </div>
                </div>
            </div>
        </div>
      </div>      
      <!-- /section 3 -->
 
<!-- Content -->

<?php get_footer();?>

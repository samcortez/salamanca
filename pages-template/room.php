<?php /* Template Name: Pagina Room */ ?>
<?php /* Template description: Para jugar con elementor */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

<!-- Content -->

    <!-- portada-->
    <div class="rojas__bg" style="background-image: url('<?= InPage::imgMod('background_rojas',''); ?>');">
        <div class="container__salamanca">
            <div class="d-flex flex-wrap rojas__box">
                <div class="col-12 col-md-4 rojas__padding_1">
                    <div class="d-flex flex-column">
                        <div class="rojas__box_title">
                            <h2 class="rojas__title">La Suite Roja</h2>
                            <div class="rojas__line"></div>
                        </div>
                        <div class="rojas__box_img_vertical">
                            <img src="<?= InPage::imgMod('room_portada_suites_1','suites-1.jpeg'); ?>" class="rojas__img" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8 rojas__padding_2">
                    <div class="rojas__box_img_horizontal">
                        <img src="<?= InPage::imgMod('room_portada_suites_1','suites-1.jpeg'); ?>" class="rojas__img" alt="">
                    </div>
                    <div class="d-flex flex-wrap rojas__box_imgs_cuadrados">
                    <div class="rojas__box_img_cuadrado col-12 col-sm-6 pl-0 rojas__padding_cuadrados_1">
                        <img src="<?= InPage::imgMod('room_portada_suites_2','suites-2.jpg'); ?>" class="rojas__img" alt="">
                    </div>
                    <div class="rojas__box_img_cuadrado col-12 col-sm-6 pr-0 rojas__padding_cuadrados_2">
                        <img src="<?= InPage::imgMod('room_portada_suites_1','suites-1.jpeg'); ?>" class="rojas__img" alt="">
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      <!--/portada-->

      <!-- section 1 -->
    <div class="reserva__bg" style="background-image: url('<?= InPage::imgMod('room_seccion_1_fondo',''); ?>');">
        <div class="container__salamanca">
            <h2 class="reserva__title"><?=InPage::__('room_title_section_1','Lorem ipsum odor')?></h2>
            <div class="reserva__line_title"></div>
            <div class="d-flex flex-wrap">
                <div class="col-12 col-md-8 reserva__col_1">
                    <div class="reserva__box_text justify-content-center text-center">
                        <p class="text-justify reserva__parrafo"><?=InPage::__('room_text_1_section_1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. <br><br> Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborumculpa qui officia deser nt, sunt in culpa qui officiar.')?></p>
                        <a href="#" class="reserva__btn reserva__none_movil"><?=InPage::__('room_btn_section_1','Reserva ya')?></a>
                    </div>
                </div>
                <div class="col-12 col-md-4 reserva__col">
                    <div class="reserva__bg_shadow">
                        <div class="reserva__bg2 text-center">
                            <h3 class="reserva__subtitle"><?=InPage::__('room_section_1_title_2','Caraterísticas')?></h3>
                            <div class="reserva__line_caracter"></div>
                            <div class="d-flex flex-wrap reserva__box_icon">
                                <div class="col-md-12 col-sm-6 col-12 px-0 my-2 d-flex flex-nowrap">
                                    <div class="px-0 d-flex ">
                                        <img src="<?= InPage::imgMod('room_seccion_1_icon_1','icon-4.png'); ?>" class="reserva__icon" alt="">
                                    </div>
                                    <div class="px-0 d-flex justify-content-center flex-column mx-md-auto">
                                        <h6 class="reserva__title_caracter"><?=InPage::__('room_section_1_subtitle_1','Lorem ipsum')?></h6>
                                        <span class="reserva__parrafo_caracter"><?=InPage::__('room_section_1_text_1','Odor ipsu am')?></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-6 col-12 px-0 my-2 d-flex flex-nowrap">
                                    <div class="px-0 d-flex ">
                                        <img src="<?= InPage::imgMod('room_seccion_1_icon_2','icon-2.png'); ?>" class="reserva__icon" alt="">
                                    </div>
                                    <div class="px-0 d-flex justify-content-center flex-column mx-md-auto">
                                        <h6 class="reserva__title_caracter"><?=InPage::__('room_section_1_subtitle_2','Lorem ipsum')?></h6>
                                        <span class="reserva__parrafo_caracter"><?=InPage::__('room_section_1_text_2','Odor ipsu am')?></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12 px-0 my-2 d-flex flex-nowrap">
                                    <div class="px-0 d-flex ">
                                        <img src="<?= InPage::imgMod('room_seccion_1_icon_3','icon-3.png'); ?>" class="reserva__icon" alt="">
                                    </div>
                                    <div class="px-0 d-flex justify-content-center flex-column mx-md-auto">
                                        <h6 class="reserva__title_caracter"><?=InPage::__('room_section_1_subtitle_3','Lorem ipsum')?></h6>
                                        <span class="reserva__parrafo_caracter"><?=InPage::__('room_section_1_text_3','Odor ipsu am')?></span>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="reserva__btn reserva__none_pc"><?=InPage::__('room_section_1_btn','Reserva ya')?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>      
    <!-- /section 1 -->

      <!-- section 2 -->
    <div class="container__salamanca d-flex flex-column position-relative ">
        <div class="habitacion__line  mx-auto"></div>
        <h2 class="text-center habitacion__title"><?=InPage::__('room_section_2_title','¿Quieres ver mas habitacion?')?></h2>
    </div>
    <div class="container__salamanca habitacion__box">
        <div class="d-flex flex-wrap position-relative">
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitacion__col pr-md-2  col-12 my-2">
                    <div class="position-relative habitacion__box_imgs">
                        <img src="<?= InPage::imgMod('room_seccion_2_collage_1','collage-9.jpg'); ?>" class="habitacion__imgs" alt="">
                        <a href="#" class="position-absolute habitacion__box_link">
                            <div class="p-3">
                                <p class="habitacion__number"><?=InPage::__('room_section_2_number_1','01')?></p>
                                <div class="habitacion__line_number"></div>
                            </div>
                            <div class="my-auto">
                                <div class="d-flex justify-content-center habitacion__box_titles">
                                <h3 class="text-center text-white my-auto habitacion__fonts_titles"><?=InPage::__('room_section_2_subtitle_1','Rooms 1')?></h3>
                            </div>
                            </div>
                        </a>
                    </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitacion__col px-md-2  col-12 my-2">
                    <div class="position-relative habitacion__box_imgs">
                        <img src="<?= InPage::imgMod('room_seccion_2_collage_2','collage-2.jpg'); ?>" class="habitacion__imgs" alt="">
                        <a href="#" class="position-absolute habitacion__box_link">
                            <div class="p-3">
                                <p class="habitacion__number"><?=InPage::__('room_section_2_number_2','02')?></p>
                                <div class="habitacion__line_number"></div>
                            </div>
                            <div class="my-auto">
                                <div class="d-flex justify-content-center habitacion__box_titles">
                                <h3 class="text-center text-white my-auto habitacion__fonts_titles"><?=InPage::__('room_section_2_subtitle_2','Rooms 2')?></h3>
                            </div>
                            </div>
                        </a>
                    </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-12 habitacion__col pl-md-2  col-12 my-2">
                    <div class="position-relative habitacion__box_imgs">
                         <img src="<?= InPage::imgMod('room_seccion_2_collage_3','collage-10.jpg'); ?>" class="habitacion__imgs" alt="">
                        <a href="#" class="position-absolute habitacion__box_link">
                            <div class="p-3">
                                <p class="habitacion__number"><?=InPage::__('room_section_2_number_3','03')?></p>
                                <div class="habitacion__line_number"></div>
                            </div>
                            <div class="my-auto">
                                <div class="d-flex justify-content-center habitacion__box_titles">
                                <h3 class="text-center text-white my-auto habitacion__fonts_titles"><?=InPage::__('room_section_2_subtitle_3','Rooms 3')?></h3>
                            </div>
                            </div>
                        </a>
                    </div>
            </div>
            <div class="position-absolute habitacion__box_black">
                
            </div>
        </div>
    </div>      
    <!-- /section 2 -->

<!-- Content -->

<?php get_footer();?>

<!DOCTYPE html>
<?php use Opalo\Helpers\InPage; ?>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <!-- Meta tags here! -->
  <title><?= InPage::pageTitle(); ?></title>
  <meta name="description" content="<?php bloginfo('description'); ?>" />
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?= InPage::noticesHandler(); ?>

  <!-- Navbar -->
    <nav class="bg-dark navbar__bg">
      <div class="container__salamanca navbar navbar-expand-lg navbar-dark navbar__shadow">
        <a class="navbar-brand navbar__box_title" href="#"><div class="d-flex flex-nowrap navbar__height">
          <h1 class="navbar__title">HS</h1>
          <div class="navbar__line_title"></div>
          <p class="navbar__title_text">Salamanca<br>Home</p>
        </div></a>
        <button class="navbar-toggler navbar__btn" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link navbar__link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link navbar__link" href="#">Nosotros</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navbar__link" href="#">Política</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navbar__link" href="#">Reserva</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navbar__link" href="#">Rooms</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navbar__link" href="#">Salamanca</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- /Navbar -->
